package pages;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.ConfigProvider;

public class HomePage extends BasePage {

    @FindBy(id = "menu-item-50")
    private WebElement myAccountLink;

    @FindBy(xpath = "//img[@alt='Shop Selenium Books']")
    private WebElement banner;

    @FindBy(xpath = "//a[@id='menu-icon']")
    private WebElement mobileMenu;

    @FindBy(id = "menu-item-40")
    private WebElement shopLink;

    public HomePage() {
        driver.get(ConfigProvider.url);
        PageFactory.initElements(driver, this);
        wait.until(ExpectedConditions.visibilityOf(banner));
    }

    @Step("Open link 'My account'")
    public SignInPage clickMyAccount() {
        myAccountLink.click();
        return new SignInPage();
    }

    @Step("Open link 'Shop'")
    public ShopPage clickShop() {
        shopLink.click();
        return new ShopPage();
    }

    public Boolean isMobileMode() {
        Allure.getLifecycle().addAttachment("ViewMode",
                "image/png",
                "png",
                screenShooter.getScreenshotAs(OutputType.BYTES));
        return mobileMenu.isDisplayed();
    }



}
