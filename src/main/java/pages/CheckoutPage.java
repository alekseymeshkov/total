package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPage extends BasePage {

    @FindBy(id = "billing_first_name")
    private WebElement firstNameField;

    @FindBy(id = "billing_last_name")
    private WebElement lastNameField;

    @FindBy(id = "billing_email")
    private WebElement emailField;

    @FindBy(id = "billing_phone")
    private WebElement phoneField;

    @FindBy(id = "billing_address_1")
    private WebElement streetField;

    @FindBy(id = "billing_city")
    private WebElement cityField;

    @FindBy(id = "billing_state")
    private WebElement countryField;

    @FindBy(id = "billing_postcode")
    private WebElement postcodeField;

    @FindBy(id = "place_order")
    private WebElement placeOrderButton;

    public CheckoutPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Fill first name")
    public CheckoutPage fillFirstName(String name) {
        firstNameField.sendKeys(name);
        return new CheckoutPage();
    }

    @Step("Fill last name")
    public CheckoutPage fillLastName(String lastName) {
        lastNameField.sendKeys(lastName);
        return new CheckoutPage();
    }

    @Step("Fill email")
    public CheckoutPage fillEmail(String email) {
        emailField.sendKeys(email);
        return new CheckoutPage();
    }

    @Step("Fill phone")
    public CheckoutPage fillPhone(String phone) {
        phoneField.sendKeys(phone);
        return new CheckoutPage();
    }

    @Step("Fill street")
    public CheckoutPage fillStreet(String street) {
        streetField.sendKeys(street);
        return new CheckoutPage();
    }

    @Step("Fill city")
    public CheckoutPage fillCity(String city) {
        cityField.sendKeys(city);
        return new CheckoutPage();
    }

    @Step("Fill country")
    public CheckoutPage fillCountry(String country) {
        countryField.sendKeys(country);
        return new CheckoutPage();
    }

    @Step("Fill postcode")
    public CheckoutPage fillPostcode(String postcode) {
        postcodeField.sendKeys(postcode);
        return new CheckoutPage();
    }

    @Step("Click 'place order'")
    public OrderConfirmationPage clickPlaceOrderButton() {
        placeOrderButton.click();
        return new OrderConfirmationPage();
    }
}
