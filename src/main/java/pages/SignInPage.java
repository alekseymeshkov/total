package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage extends BasePage {

    @FindBy(id = "reg_email")
    private WebElement registerEmail;

    @FindBy(id = "reg_password")
    private WebElement registerPassword;

    @FindBy(xpath = "//input[@name='register']")
    private WebElement registerButton;

    @FindBy(xpath = "//strong/..")
    private WebElement errorMessage;

    public SignInPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Fill email")
    public SignInPage fillEmail(String email) {
        registerEmail.sendKeys(email);
        return this;
    }

    @Step("Fill password")
    public SignInPage fillPassword(String pass) {
        js.executeScript("document.getElementById('reg_password').setAttribute('value', '" + pass + "')");
        return this;
    }

    @Step("Click register button")
    public SignInPage clickRegister() {
        registerButton.click();
        return this;
    }

    @Step("Get error message")
    public String getErrorMessage() {
        return errorMessage.getText();
    }
}
