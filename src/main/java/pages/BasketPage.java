package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasketPage extends BasePage {

    @FindBy(xpath = "//td[@data-title='Subtotal']/span")
    private WebElement subtotalPrice;

    @FindBy(xpath = "//tr[@class='order-total']//span")
    private WebElement totalPrice;

    @FindBy(xpath = "//a[@class='checkout-button button alt wc-forward']")
    private WebElement proceedToCheckoutButton;

    public BasketPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Get subtotal price")
    public double getSubTotalPrice() {
        return Double.parseDouble(
                subtotalPrice.getText().replaceAll("₹", ""));
    }

    @Step("Get total price")
    public double getTotalPrice() {
        return Double.parseDouble(
                totalPrice.getText().replaceAll("₹", ""));
    }

    @Step("Click 'procceed to checkout button'")
    public CheckoutPage clickProceedToCheckoutButton() {
        proceedToCheckoutButton.click();
        return new CheckoutPage();
    }

}
