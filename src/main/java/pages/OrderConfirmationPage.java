package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderConfirmationPage extends BasePage {

    @FindBy(xpath = "//p[@class=\"woocommerce-thankyou-order-received\"]")
    private WebElement successOrderMessage;

    public OrderConfirmationPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Get success order message")
    public String getSuccessOrderMessage() {
        return successOrderMessage.getText();
    }
}
