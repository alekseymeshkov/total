package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement;

public class ShopPage extends BasePage {

    @FindBy(xpath = "//span[@class='ui-slider-handle ui-state-default ui-corner-all'][1]")
    private WebElement leftPriceSlider;

    @FindBy(xpath = "//span[@class='ui-slider-handle ui-state-default ui-corner-all'][2]")
    private WebElement rightPriceSlider;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement filterButton;

    @FindBy(xpath = "//span[@class='price']/span[@class='woocommerce-Price-amount amount']")
    private List<WebElement> artPrices;

    @FindBy(xpath = "//span[@class='price']/ins/span[@class='woocommerce-Price-amount amount']")
    private List<WebElement> artPricesNew;

    @FindBy(xpath = "//h4")
    private WebElement someHeader;

    @FindBy(xpath = "//li[contains(@class, 'product')]//h3")
    private List<WebElement> artNames;

    @FindBy(xpath = "//a[text()='Add to basket']")
    private List<WebElement> addToBasketButtons;

    @FindBy(xpath = "//span[@class='cartcontents']")
    private WebElement basketArtCounter;

    @FindBy(xpath = "//span[@class='amount']")
    private WebElement basketArtPrice;

    @FindBy(id = "wpmenucartli")
    private WebElement basketButton;

    public ShopPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Adjust left filter slider to selected price")
    public ShopPage setLeftPriceSlider(int price) {
        leftPriceSlider.click();
        for (int i = 0; i < price - 150; i++) {
            actions.sendKeys(Keys.ARROW_RIGHT).perform();
        }
        unFocus();
        return this;
    }

    @Step("Adjust right filter slider to selected price")
    public ShopPage setRightPriceSlider(int price) {
        rightPriceSlider.click();
        for (int i = 0; i < 500 - price; i++) {
            actions.sendKeys(Keys.ARROW_LEFT).perform();
        }
        unFocus();
        return this;
    }

    @Step("Click on filter button")
    public ShopPage clickFilter() {
        filterButton.click();
        return this;
    }

    @Step("Getting all art prices")
    public List<Double> getArtPrices() {
        List<WebElement> WebElementPrices = new ArrayList<>(artPrices);
        WebElementPrices.addAll(artPricesNew);

        List<Double> prices = new ArrayList<>();
        for (WebElement price : WebElementPrices) {
            prices.add(Double.parseDouble(price.getText().replaceAll("₹", "")));
        }
        return prices;
    }

    @Step("Click on art by name")
    public ArtPage clickArtByName(String name) {
        for (WebElement art : artNames) {
            if (art.getText().equals(name)) {
                art.click();
                return new ArtPage();
            }
        }
        return null;
    }

    @Step("Get random art name")
    public String getRandomArtName() {
        return artNames.get(new Random().nextInt(artNames.size())).getText();
    }

    @Step("Click 'add to basket' button on random art")
    public ShopPage addRandomArtToBasket() {
        String basketAmount = basketArtCounter.getText();
        addToBasketButtons.get(new Random().nextInt(addToBasketButtons.size())).click();
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(basketArtCounter, basketAmount)));
        return this;
    }

    @Step("Get count of arts in basket")
    public int getArtsCountInBasket() {
        return Integer.parseInt(basketArtCounter.getText().split(" ")[0]);
    }

    @Step("Get total price of arts in basket")
    public double getTotalPriceInBasket() {
        return Double.parseDouble(
                basketArtPrice.getText().replaceAll("₹", ""));
    }

    @Step("Go to basket")
    public BasketPage clickBasket() {
        basketButton.click();
        return new BasketPage();
    }

    public void unFocus() {
        someHeader.click();
    }
}
