package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArtPage extends BasePage {

    @FindBy(xpath = "//h1")
    private WebElement artName;

    public ArtPage() {
        PageFactory.initElements(driver, this);
    }

    public String getArtName() {
        return artName.getText();
    }
}
