package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends BasePage {

    @FindBy(xpath = "//a[text()='Sign out']")
    private WebElement signOutLink;

    public MyAccountPage() {
        PageFactory.initElements(driver, this);
    }

    public Boolean isLoggedIn() {
        return signOutLink.isDisplayed();
    }

}
