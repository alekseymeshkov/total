package utils;

import java.security.SecureRandom;

public abstract class Generator {

    private static SecureRandom rand = new SecureRandom();
    private static StringBuilder sb = new StringBuilder();

    public static String generateEmail() {
        String email = rand.nextInt(1000) + "@test.com";
        return email;
    }

    public static String generatePass(int len)
    {
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (int i = 0; i < len; i++)
        {
            int randomIndex = rand.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex));
        }

        return sb.toString();
    }
}
