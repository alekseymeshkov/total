package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.Dimension;
import pages.HomePage;
import utils.Listener;

@DisplayName("Mobile view tests")
@Epic(value = "Diploma work")
@Feature(value = "Web Tests")
@ExtendWith(Listener.class)
public class MobileViewTests extends BaseTest {

    @Test
    @Story(value = "Mobile")
    @DisplayName("Mobile view on test")
    @Description(value = "Scale browser window to resolution 1000x1080 and checking whether mobile view is displayed")
    public void mobileModeOnTest() {
        driver.manage().window().setSize(new Dimension(1000, 1080));
        Assertions.
                assertThat(new HomePage()
                        .isMobileMode())
                .as("Check mobile view is on")
                .isTrue();
    }

    @Test
    @Story(value = "Mobile")
    @DisplayName("Mobile view off test")
    @Description(value = "Open site in fullscreen and check it is not mobile view")
    public void mobileModeOffTest() {
        Assertions.
                assertThat(new HomePage()
                        .isMobileMode())
                .as("Check mobile view is off")
                .isFalse();
    }
}
