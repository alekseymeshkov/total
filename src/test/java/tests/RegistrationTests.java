package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import net.joshka.junit.json.params.JsonFileSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import pages.HomePage;
import pages.MyAccountPage;
import utils.Generator;
import utils.Listener;

import javax.json.JsonObject;

@DisplayName("Registration tests")
@Epic(value = "Diploma work")
@Feature(value = "Web Tests")
@ExtendWith(Listener.class)
public class RegistrationTests extends BaseTest {

    @Test
    @Story(value = "Register")
    @DisplayName("Positive account registration test")
    @Description(value = "Open registration page and trying to register with random data")
    public void positive_account_registration_test() {
        new HomePage()
                .clickMyAccount()
                .fillEmail(Generator.generateEmail())
                .fillPassword(Generator.generatePass(8))
                .clickRegister();
        Assertions
                .assertThat(new MyAccountPage()
                        .isLoggedIn())
                .as("Successful registration")
                .isTrue();
    }

    @ParameterizedTest
    @Story(value = "Register")
    @DisplayName("4 negative account registration tests")
    @Description(value = "Open registration page and trying to register with different invalid data 4 times")
    @JsonFileSource(resources = "/negative_account_registration_data.json")
    public void negative_account_registration_test(JsonObject invalid_data) {
        Assertions
                .assertThat(new HomePage()
                        .clickMyAccount()
                        .fillEmail(invalid_data.getString("email"))
                        .fillPassword(invalid_data.getString("password"))
                        .clickRegister()
                        .getErrorMessage())
                .isEqualTo(invalid_data.getString("message"));
    }
}
