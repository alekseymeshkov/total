package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import net.joshka.junit.json.params.JsonFileSource;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import pages.BasketPage;
import pages.HomePage;
import pages.ShopPage;
import utils.Listener;

import javax.json.JsonObject;
import java.util.List;

@DisplayName("Shop tests")
@Epic("Diploma work")
@Feature("Web Tests")
@ExtendWith(Listener.class)
public class ShopTests extends BaseTest{

    @Test
    @Story("Filter by price functionality")
    @DisplayName("Filter test")
    @Description("Open shop menu, adjust filter, checking results")
    public void filterTest() {
        List<Double> prices = new HomePage()
                .clickShop()
                .setLeftPriceSlider(200)
                .setRightPriceSlider(450)
                .clickFilter()
                .getArtPrices();

        for (Double price : prices) {
            Assertions.
                    assertThat(price >= 200 && price <= 450)
                    .as("Checking each art has price >= 200 and price <= 450")
                    .isTrue();
        }
    }

    @Test
    @Story("Product Categories Functionality")
    @DisplayName("Open art test")
    @Description("Open shop menu, get some art name, click on this art, check if name that was clicked is equals name on art page")
    public void openArtTest() {
        String clickedArtName = new HomePage()
                .clickShop()
                .getRandomArtName();
        String artName = new ShopPage()
                .clickArtByName(clickedArtName)
                .getArtName();
        Assertions.assertThat(clickedArtName).isEqualTo(artName);
    }

    @ParameterizedTest
    @Story("View basket Functionality")
    @DisplayName("Add to basket test")
    @Description("Click shop menu, add random art to basket, click basket, click proceed to checkout, take an order")
    @JsonFileSource(resources = "/orderData.json")
    public void addToBasketTest(JsonObject orderData) {
        int artsInBasket = 1;
        new HomePage()
                .clickShop()
                .addRandomArtToBasket();
        Assertions.assertThat(new ShopPage().getArtsCountInBasket()).isEqualTo(artsInBasket);
        Assertions.assertThat(new ShopPage().getTotalPriceInBasket()).isGreaterThan(0);

        new ShopPage().clickBasket();
        Double totalPrice = new BasketPage().getTotalPrice();
        Double subTotalPrice = new BasketPage().getSubTotalPrice();
        Assertions.assertThat(totalPrice).isGreaterThan(subTotalPrice);

        Assertions.assertThat(new BasketPage()
                .clickProceedToCheckoutButton()
                .fillFirstName(orderData.getString("first_name"))
                .fillLastName(orderData.getString("last_name"))
                .fillEmail(orderData.getString("email"))
                .fillPhone(orderData.getString("phone"))
                .fillStreet(orderData.getString("street"))
                .fillCity(orderData.getString("city"))
                .fillCountry(orderData.getString("country"))
                .fillPostcode(orderData.getString("postcode"))
                .clickPlaceOrderButton()
                .getSuccessOrderMessage())
                .isEqualTo("Thank you. Your order has been received.");
    }
}
